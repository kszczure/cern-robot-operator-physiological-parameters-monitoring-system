# BioSignal Operator Monitoring

1) Install python 3.8.8 version (https://www.python.org/downloads/release/python-388/).
   Remember during the installation to enable "Add Python 3.8 to PATH" checkbox.
2) Run setup.py file in order to install packages and dependencies (flask, scipy, scipy.signal). You can use Visual Studio Code program.
3) Go to environmental variables and check python Path (right click on This PC >> Properties >> Advanced system settings >> Environment Variables).
   Create a New Path (python path + "\Scripts")


To run the code, write flask run in the right path in the Command Prompt
(i.e. C:\Users\UserName\Documents\biosignal-operator-monitoring> flask run)

You can give commands using HTTP protocol, as written in the Documentation pdf in the API section.

CHANNEL 1 = RESPIRATION sensor (belt PTZ);
CHANNEL 2 = ECG sensor;
CHANNEL 3 = EDA sensor;
