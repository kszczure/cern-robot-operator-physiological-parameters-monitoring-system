import os
import threading
import time
from biosignalplux import BioSignalPlux 

class OperatorMonitoring():

    # create the plux device and setup it
    def __init__(self, address, freq, code):
        self.biosignalPlux = BioSignalPlux(address)
        self.biosignalPlux.frequency = freq
        self.biosignalPlux.channel_number = code
        self.acquisition_stopped = False
        self.on = False
        self.stopwatch = time.perf_counter()
        self.savepath = os.getcwd() + "/Results/"

    # start acquisition
    def start(self):
        if (self.on):
            raise Exception("The OperatorMonitoring is already on")

        self.biosignalPlux.cancelationRequested = False
        self.biosignalPlux.start()
        self.thread = threading.Thread(target=self.biosignalPlux.loop)
        self.thread.start()
        self.on = True
        print("start")

    # stop acquisition
    def stop(self):
        if (not self.on):
            raise Exception("The OperatorMonitoring is already off")

        self.biosignalPlux.cancelationRequested = True
        self.on = False
        self.thread.join()
        self.biosignalPlux.stop()
        self.biosignalPlux.auto_saving_first_pass = True
        self.biosignalPlux.gui_sent_data_saving_first_pass = True
        #self.biosignalPlux.close()
        print("stop")

    # This method return the data from the plux device depending of the parameters
    def getBaselineInformation(self):
        if (not self.biosignalPlux.isBaselineAcquired()):
            raise Exception
        baseline = { 
            "baselineHeartRate": self.biosignalPlux.baseline_HR,
            "baselineRespirationRate": self.biosignalPlux.baseline_RR,
            "baselineEdaPeaks": self.biosignalPlux.baseline_EDA_peaks
         }
        return baseline

    def getRealTimeVitalParameters(self):
        realTimeParameters = {
            "heartRate": self.biosignalPlux.HR,
            "respirationRate": self.biosignalPlux.RR,
            "edaPeaks": self.biosignalPlux.EDA_peaks,
            "operatorState": self.biosignalPlux.operator_condition
        }
        return realTimeParameters

    def saveResult(self, filename):
        filePath = self.savepath + filename + ".csv"

        if (not os.path.exists(self.savepath)):
            raise Exception()
        if (os.path.isfile(filePath)):
            raise FileExistsError()
        result = ""
        header = "global_resp, global_ecg, global_eda"
        result += header + "\n"
        
        for i in range(len(self.biosignalPlux.global_resp)):
            result += str(self.biosignalPlux.global_resp[i]) + "," + str(self.biosignalPlux.global_ecg[i]) + "," + str(self.biosignalPlux.global_eda[i]) + "\n"
        with open(filePath, "w") as f:
            f.write(result)


    def deviceInfo(self):       
        deviceInfo = {
            "status": "ON" if self.on == True else "OFF",
            "batteryPercentage": self.biosignalPlux.battery,
            "isBaselineAcquired": self.biosignalPlux.isBaselineAcquired()
        }
        return deviceInfo        