from datetime import datetime
import platform
import sys
import scipy as sp
from scipy.signal import butter,filtfilt
import time
from datetime import datetime
import os
import time
import plux

osDic = {"Darwin": "MacOS",
         "Linux": "Linux64",
         "Windows":("Win32","Win64")}
if platform.system() != "Windows":
    sys.path.append("PLUX-API-Python3/{}/plux.so".format(osDic[platform.system()]))
else:
    if platform.architecture()[0] == '64bit':
        sys.path.append("PLUX-API-Python3/Win64_38")
    else:
        sys.path.append("PLUX-API-Python3/Win32")


class BioSignalPlux(plux.SignalsDev):
    def __init__(self, address):
        plux.MemoryDev.__init__(address)
        plux.BaseDev.__init__(address)
        # device = BioSignalPlux(address)
        self.channel_number = ''
        self.frequency = 0
        self.data_resp = []
        self.data_ecg = []
        self.data_eda = []
        self.number_bits = 16 
        self.baseline_HR = -1
        self.baseline_RR = -1
        self.baseline_EDA_peaks = -1
        self.go = time.time()
        self.acquisition_stopped = False

        self.HR = 0
        self.RR = 0
        self.EDA_peaks = 0

        self.global_resp = []
        self.global_ecg = []
        self.global_eda = []

        self.interval_resp = []
        self.interval_ecg = []
        self.interval_eda = []

        self.fs = 250.0      # sample rate, Hz
        self.cutoff = 0.1    # desired cutoff frequency of the filter, Hz 
        self.nyq = 0.5 * self.fs  # Nyquist Frequency
        self.order = 2       # sin wave can be approx represented as quadratic
        self.battery = self.batteryPercentage()
        self.cancelationRequested = False
        self.result = ''
        self.resultGUI = ''
        self.auto_saving_first_pass = True
        self.gui_sent_data_saving_first_pass = True
        

    def start(self):
        self.baseline_HR = -1
        self.baseline_RR = -1
        self.baseline_EDA_peaks = -1
        self.data_resp = []
        self.data_ecg = []
        self.data_eda = []
        self.global_resp = []
        self.global_ecg = []
        self.global_eda = []
        plux.SignalsDev.start(self, self.frequency, self.channel_number, 16)
        self.t = time.ctime()
        self.file_name_datetime_suffix = time.strftime("%Y%m%d-%H%M%S")

    def onRawFrame(self, nSeq, data):
        if (self.cancelationRequested):
            return True

        self.convertRawDataRespirationRate(data[0])
        self.convertRawDataHeartRate(data[1])
        self.convertRawDataSkinActivity(data[2])

        if (self.isBaselineAcquired() and self.isDataArrayFilled()):
            self.runAnalysis()
            self.autoSaving()
            self.guiSentDataSaving()
        elif (self.canBaselineBeAcquired() and self.isDataArrayFilled()):
            self.getBaselineData()
        
        return False

    def runAnalysis(self):
        # start = time.time_ns()

        breath, properties = sp.signal.find_peaks(self.data_resp, prominence = 0.1, distance=350, height=(None,0.7))
        self.RR = len(breath)
        
        beats = sp.signal.find_peaks(self.data_ecg, prominence=0.09, distance=130, height=0.06, threshold=(None,1)) # for 250Hz frequency
        self.HR = len(beats[0]) # type(beats) is a list, so take the first element in 0
        
        EDA = sp.signal.medfilt(self.data_eda)
        normal_cutoff = self.cutoff / self.nyq
        b, a = butter(self.order, normal_cutoff, btype='low', analog=False)
        SCL = filtfilt(b, a, self.data_eda)
        event = sp.signal.find_peaks(EDA-SCL, height=0.06, distance=800) 
        self.EDA_peaks = len(event[0]) 


        # HEART RATE 
    
        if self.HR>=(self.baseline_HR + 0) and self.HR<=(self.baseline_HR + 0.25*self.baseline_HR): 
            self.HR_state = ((self.HR - self.baseline_HR)/(self.denHR))
        elif self.HR>(self.baseline_HR + 0.25*self.baseline_HR):
            self.HR_state = 1
        else:
            self.HR_state = 0

        # RESPIRATION RATE

        if self.RR>=(self.baseline_RR + 0) and self.RR<=(self.baseline_RR + 0.2*self.baseline_RR):
            self.RR_state = ((self.RR - self.baseline_RR)/(self.denRR))
        elif self.RR>(self.baseline_RR + 0.2*self.baseline_RR):
            self.RR_state = 1
        else:
            self.RR_state = 0

        # ELECTRODERMAL ACTIVITY PEAKS

        if self.EDA_peaks>=(self.baseline_EDA_peaks + 3) and self.EDA_peaks<=(self.baseline_EDA_peaks + 6):
            self.eda_state = ((self.EDA_peaks - (self.baseline_EDA_peaks + 3))/(self.denEDA))
        elif self.EDA_peaks>(self.baseline_EDA_peaks + 6):
            self.EDA_state = 1
        else:
            self.EDA_state = 0

        # OPERATOR CONDITION

        self.state = 0.5*self.HR_state + 0.4*self.RR_state + 0.1*self.EDA_state

        if self.state<=0.3:
            self.operator_condition = 'Relaxed'
        elif self.state>0.3 and self.state<=0.65:
            self.operator_condition = 'Standard'
        elif self.state>0.65:
            self.operator_condition = 'Stressed'

        if self.HR <= 30:
           print("/!\ /!\ /!\ Error in ECG detection: check ecg sensors and electrodes!")

        print("Heart Rate: ", self.HR, "; Respiration Rate: ", self.RR, "; EDA peaks: ", self.EDA_peaks, "; OPERATOR STATE: ", self.operator_condition )

        del self.data_resp[0:1250] # remove first 5 seconds
        del self.data_ecg[0:1250] # remove first 5 seconds
        del self.data_eda[0:1250] # remove first 5 seconds

        # end = time.time_ns()
        # print("runAnalysis executionTime: " + str(end - start))

    def getBaselineData(self):
        
        breath, properties = sp.signal.find_peaks(self.data_resp, prominence= 0.1 , distance=350, height=(None,0.7)) # prominence = 0.03 is the threshold to remove noise
        RR = len(breath)
        
        beats = sp.signal.find_peaks(self.data_ecg, prominence=0.09 , distance=130, height=0.06, threshold=(None,1)) # for 250Hz frequency
        HR = len(beats[0]) # type(beats) is a list, so take the first element in 0
        
        
        EDA = sp.signal.medfilt(self.data_eda)
        normal_cutoff = self.cutoff / self.nyq
        b, a = butter(self.order, normal_cutoff, btype='low', analog=False)
        SCL = filtfilt(b, a, self.data_eda)
        event = sp.signal.find_peaks(EDA-SCL, height=0.06, distance=800) 
        EDA_peaks = len(event[0])

        print("This value is your heart rate baseline: ", HR)
        print("This value is your respiration rate baseline: ", RR)
        print("This value is your number of EDA peaks baseline: ", EDA_peaks)
        
        self.baseline_HR = HR # hold this
        self.baseline_RR = RR # hold this
        self.baseline_EDA_peaks = EDA_peaks # hold this
        self.HR = self.baseline_HR
        self.RR = self.baseline_RR
        self.EDA_peaks = self.baseline_EDA_peaks
        self.operator_condition = 'Standard'
        
        self.denHR = 0.25*self.baseline_HR
        
        if self.baseline_HR == 0 or self.baseline_HR <= 30:
            print("/!\ /!\ /!\ Error in ECG detection: check ecg sensors and electrodes!") 
            self.denHR = 60

        self.denRR = 0.20*self.baseline_RR

        if self.baseline_RR == 0 or self.baseline_RR<=3:
            print("/!\ /!\ /!\ Error in breathing detection: check respiration belt at chest level!") 
            self.denRR = 15

        self.denEDA = 3

        del self.data_resp[0:1250] # remove first 5 seconds
        del self.data_ecg[0:1250]  # remove first 5 seconds
        del self.data_eda[0:1250]  # remove first 5 seconds

    def autoSaving(self):
        # start = time.time_ns()

        self.savepath = os.getcwd() + "/Results/"
        self.filePath = self.savepath + "raw_physio_data_" +self.file_name_datetime_suffix  + ".csv"

        newResult = ""
        if(self.auto_saving_first_pass):
            header = "Time stamp, global_resp, global_ecg, global_eda"
            startTime = self.t
            newResult += startTime + "\n" 
            newResult += header + "\n" 

        for i in range(len(self.interval_resp)):
            newResult += str(time.ctime()) + "," + str(self.interval_resp[i]) + "," + str(self.interval_ecg[i]) + "," + str(self.interval_eda[i]) + "\n"
            
        self.result += newResult
        with open(self.filePath, "a") as f:
            f.write(newResult)
        f.close()

        self.interval_resp = []
        self.interval_ecg = []
        self.interval_eda = []

        self.auto_saving_first_pass = False

        # end = time.time_ns()
        # print("autoSaving executionTime: " + str(end - start))

    def guiSentDataSaving(self):
        # start = time.time_ns()

        self.savepath = os.getcwd() + "/Results/"
        self.filePath = self.savepath + "GUI_physio_data_" + self.file_name_datetime_suffix  + ".csv"

        newResult = ""
        if(self.gui_sent_data_saving_first_pass):
            header = "Time stamp, RR, HR, EDA"
            startTime = self.t
            newResult += startTime + "\n" 
            newResult += header + "\n" 

        newResult += str(time.ctime()) + "," + str(self.RR) + "," + str(self.HR) + "," + str(self.EDA_peaks) + "\n"
        self.resultGUI += newResult
        with open(self.filePath, "a") as f:
            f.write(newResult)
        f.close()

        self.gui_sent_data_saving_first_pass = False

        # end = time.time_ns()
        # print("guiSentDataSaving executionTime: " + str(end - start))
        

    # This method return the heart rate data from raw data
    def convertRawDataHeartRate(self, raw_data):
        ecg = -(((raw_data/(pow(2, self.number_bits))-0.5)*3)/1019)*1000   # raw data in ECG (mV) #channel2
        self.data_ecg.append(ecg)    
        self.global_ecg.append(ecg)
        self.interval_ecg.append(ecg)
        return ecg

    # This method return the respiration rate data from raw data
    def convertRawDataRespirationRate(self, raw_data):
        resp = ((raw_data/(pow(2, self.number_bits)-1)-0.5)*3)   # raw data in respiration unit #channel1
        self.data_resp.append(resp)
        self.global_resp.append(resp)
        self.interval_resp.append(resp)
        return resp

    # This method return the skin activity data from raw data
    def convertRawDataSkinActivity(self, raw_data):
        eda = ((raw_data/(pow(2, self.number_bits))*3)/0.12) # raw data in microSiemens
        self.data_eda.append(eda)
        self.global_eda.append(eda)
        self.interval_eda.append(eda)
        return eda

    def isBaselineAcquired(self):
        return self.baseline_HR != -1 and self.baseline_RR != -1 and self.baseline_EDA_peaks != -1
    
    def canBaselineBeAcquired(self):
        return self.baseline_HR == -1 and self.baseline_RR == -1 and self.baseline_EDA_peaks == -1
    
    def isDataArrayFilled(self):
        return len(self.data_ecg) >= 15000 and len(self.data_resp) >= 15000 and len(self.data_eda) >= 15000 # 15000 samples = 60s

    def batteryPercentage(self):
        battery = self.getBattery()
        battery= int(battery)
        print('battery level: ' + str(battery) + '%')
        return battery