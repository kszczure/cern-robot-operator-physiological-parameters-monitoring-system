### This script is used to install the package required for the project.
## Required packages :
## - flask
## - scipy
## - scipy.signal


import pip

print("Installing packages")

try:
    import flask
except ImportError:
    print("Installing Flask")
    pip.main(['install', 'flask'])  

try:
    import scipy
except ImportError:
    print("Installing scipy")
    pip.main(['install', 'scipy'])

try:
    import scipy.signal
except ImportError:
    print("Installing scipy.signal")
    pip.main(['install', 'scipy.signal'])


print("Installation done")