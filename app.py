from flask import Flask, jsonify, Response, request
from OperatorMonitoring import OperatorMonitoring
import sys

try:
    opMonitoring = OperatorMonitoring("BTH00:07:80:4B:3E:EC", 250, 0x07)
except:
    print()
    print("/!\\ Impossible to initialize the PLUX device. Please verify that the device is ON and the bluetooth connection has been setup. /!\\")
    sys.exit(1)

print("The device has been properly initialized")

app = Flask(__name__)

print("App started")

@app.route('/baselineInformation' , methods=['GET']) # http://127.0.0.1:5000/baselineInformation
def getBaseLineInformations():
    try:
        report = opMonitoring.getBaselineInformation()  
    except:
        return jsonify({"error": "Could not get baseline informations"}), 400
    return jsonify(report)

@app.route('/realTimeVitalParameters' , methods=['GET']) # http://127.0.0.1:5000/getRealTimeVitalParameters
def getRealTimeVitalParameter():
    try:
        report = opMonitoring.getRealTimeVitalParameters()  
    except:
        return jsonify({"error": "Vital Parameters not available"}), 400
    return jsonify(report)

@app.route('/start' , methods=['GET'])
def start():
    try: 
        opMonitoring.start() 
    except:
        return jsonify({"error": "The device could not be initialized"}), 400
    return Response(status=200)

@app.route('/saveResults', methods=['POST'])
def saveResults():
    payload = request.get_json()
    if not ("filename" in payload):
        return jsonify({"error": "Json format is incorrect"}), 400
    try:
        opMonitoring.saveResult(payload["filename"])
    except FileExistsError:
        return jsonify({"error": "The file is already existing"}), 400
    except:
        return jsonify({"error": "Data not saved"}), 400
    return Response(status=200)

@app.route('/stop' , methods=['GET'])
def stop():
    if (opMonitoring.on == False):
        return jsonify({"error": "Monitoring device is already stopped"}), 400
    try:
        opMonitoring.stop() 
    except:
        return jsonify({"error": "Could not stop the monitoring device "}), 400
    return Response(status=200)

@app.route('/info' , methods=['GET'])
def info():
    try:
        report = opMonitoring.deviceInfo()
    except:
        return jsonify({"error": "Device info not avalaible"}), 400
    return jsonify(report)



